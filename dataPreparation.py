import re
import numpy as np
from wordcloud import STOPWORDS
from googletrans import Translator  # version 3.1.0a0
import pandas as pd
from keras.preprocessing.sequence import pad_sequences

# from emoji import UNICODE_EMOJI #I originally planned to use emojies as features, but it did not work well

def translateTweetsToEnglish(data):
    translator = Translator()
    data["tweets"] = data["tweets"].apply(lambda tweet: translator.translate(tweet, dest='en').text)
    return data


def standardizeText(tweet):
    text = tweet.lower()
    # remove irrelevant words which may create bias
    text = re.sub('@ ', '@', text)  # remove surplus space in mentions
    text = re.sub('# ', '#', text)  # remove surplus space in hashtags

    # text = ''.join(' ' + char if char in UNICODE_EMOJI else char for char in text).strip() #space out emojies

    text = re.sub('rt @\w+|english translation|english transcript', "", text)
    text = re.sub("#([a-zA-Z0-9_]{1,15})", "", text)  # remove hashtags
    for char in ['\u200d', '\u200d']:
        text = re.sub(char, ' ' + char + ' ', text)

    text = re.sub(r"\\", " ", text)
    text = re.sub("(?P<url>https?://[^\s]+)|(?P<url2>http?://[^\s]+)|(?P<url3>tp?://[^\s]+)", " ", text)
    text = re.sub("@([a-zA-Z0-9_]{1,15})| #([a-zA-Z0-9_]{1,15})", " ", text)
    text = re.sub("u2026 ", " ", text)
    text = re.sub(" u2026", " ", text)

    text = re.sub("ud83d ", " ", text)
    text = re.sub("2019s ", " ", text)
    text = re.sub("2019t ", " ", text)
    text = re.sub("ud83c ", " ", text)
    text = re.sub("ude02 ", " ", text)
    text = re.sub("u201d ", " ", text)
    text = re.sub("u2019m ", " ", text)
    text = re.sub("ufe0f ", " ", text)
    text = re.sub("u2026 ", " ", text)
    text = re.sub("u2019re ", " ", text)
    text = re.sub("ud83e ", " ", text)
    text = re.sub("ivotebtsbbmas ", " ", text)

    # remove unwanted characters
    for string in [r"p //t.",
                   "htt",
                   "'s "]:
        text = re.sub(string, " ", text)

    for string in ["[^\S]", '[#&.,":;!?-]', "'", r"  "]:
        text = re.sub(string, " ", text)  # remove tabs

    # fix recurring typos (some due to text standardization)
    text = re.sub(r"knw", "know", text)
    text = re.sub(r"abt", "about", text)
    text = re.sub(r" s a w ", " saw ", text)
    text = re.sub(r" i m ", " i am ", text)
    text = re.sub(r" don t ", " do not ", text)
    text = re.sub(r" we ll ", " we will ", text)
    text = re.sub(r" i ll ", " i will ", text)
    for string in [' s ', ' t ', ' n ', ' nn ']:
        text = re.sub(string, "", text)  # remove tabs
    text = re.sub(r" ... ", " ", text)
    text = re.sub("  ", " ", text)
    for stop_word in STOPWORDS:
        text = re.sub(stop_word, "", text)

    return text.strip()


def extractLinksFromTweet(tweet):
    links = re.findall("(?P<url>https?://[^\s]+)|(?P<url2>http?://[^\s]+)|(?P<url3>tp?://[^\s]+)", tweet)
    links = [''.join(link) for link in links]
    return links


def extractMentionsTweet(tweet):
    mentions = re.findall("@([a-zA-Z0-9_]{1,15})", tweet)
    return mentions


def concatDFColumns(data):
    #  Fix false parsing and concat columns
    for i in range(2, 20):
        col = 'Unnamed: ' + str(i)
        if col not in data.columns:
            break
        data[col] = data[col].apply(lambda x: '' if pd.isnull(x) else x)
        # data['content'] = data['content'] + ',' + str(data[col])
        del (data[col])
    data['tweets'] = data['content'].copy()
    del (data['content'])
    del (data['tweet'])


def buildFeatures(data):
    # using standard features
    # features distirbution for Isis and non isis was similar, so  they will not be used
    print("building features")
    data["Full Tweets"] = data['tweets'].copy()
    print("extracting links")
    data["Links"] = data["tweets"].apply(lambda tweet: extractLinksFromTweet(tweet))
    data["Mentions"] = data["tweets"].apply(lambda tweet: extractMentionsTweet(tweet))
    for char in ['#', '@', '!']:
        data[char + ' Count'] = data['tweets'].apply(lambda x: len([c for c in str(x) if c == char]))
    print("extracting characters counts")

    data['url Count'] = data["Links"].apply(lambda links_vector: len(links_vector))
    data['Mean Chars Per Words'] = data['tweets'].apply(lambda tweet: np.mean([len(word) for word in tweet.split()]))

    data['Stop Word Count'] = data['tweets'].apply(lambda tweet: len([word for word in tweet.lower().split()
                                                                      if word in STOPWORDS]))
    data['Word Count'] = data['tweets'].apply(lambda tweet: len(tweet.split()))
    data['Unique Word Count'] = data['tweets'].apply(lambda tweet: len(set(tweet.split())))
    print("finished building features")


def load_data(random_data_path, isis_data_path, translate_text=False, label_0_subsample=0.2):
    # load data
    data_Isis = pd.read_excel(random_data_path)  # note - this requires xlrd version <=1.2.0
    data_Isis['label'] = 1
    data_random = pd.read_excel(isis_data_path)
    data_random['label'] = 0  # Assume random tweets are not by Isis

    print("finished loading data")
    # drop that columns are only available for Isis tweets, and likely lead to overfit
    data_Isis.drop(columns=['description', 'name', 'username', 'time',
                            'followers', 'location', 'numberstatuses'], inplace=True)

    concatDFColumns(data_random)
    data_random.dropna(inplace=True)
    # use google translate to translate arabic tweets to english
    if translate_text:
        translateTweetsToEnglish(data_Isis)

    # data is extremely biased --> need to undersample random tweets
    data_non_isis = data_random.sample(frac=label_0_subsample, replace=False)
    data = pd.concat([data_non_isis, data_Isis], sort=False)
    print("standardizing text")
    # clean non-standard chars, and generate struct that can be tokenized
    data['tweets'] = data['tweets'].apply(lambda tweet: standardizeText(tweet))
    # no nulls in Isis data, and we have enough random tweets, so we can simply dropna
    data.dropna(inplace=True)
    return data

    #


def predict(model, tokenizer, tweet, max_n_chars=280):
    sample = pad_sequences(tokenizer.texts_to_sequences([tweet]), padding='post', maxlen=max_n_chars)
    prediction = model.predict(sample)

    return prediction
