import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from collections import defaultdict
from wordcloud import STOPWORDS


def ngrams(text, n_gram=1):
    token = [x for x in text.lower().split(' ') if x != '' if x not in STOPWORDS and not x.isdigit()]
    ngrams = zip(*[token[i:] for i in range(0, n_gram)])
    return [' '.join(ngram) for ngram in ngrams]


def inspectNgrams(data):
    """
    Inspects the different ngrams of the data
    :param data: dataframe with data
    :return:
    """
    # This inspection was used to clean data
    Isis_tweets = data['label'] == 1

    # calculate ngrams

    max_Ngrams_n = 3

    Isis_ngrams = [defaultdict(int) for x in range(0, max_Ngrams_n)]
    random_ngrams = [defaultdict(int) for x in range(0, max_Ngrams_n)]

    for i in range(0, max_Ngrams_n):
        for tweet in data[Isis_tweets]['tweets']:
            for word in ngrams(tweet, i + 1):
                Isis_ngrams[i][word] += 1

        for tweet in data[~Isis_tweets]['tweets']:
            for word in ngrams(tweet, i + 1):
                random_ngrams[i][word] += 1

    df_Isis_ngrams = [pd.DataFrame(sorted(Isis_ngrams[i].items(), key=lambda x: x[1])[::-1]) for i in
                      range(0, max_Ngrams_n)]
    df_random_ngrams = [pd.DataFrame(sorted(random_ngrams[i].items(), key=lambda x: x[1])[::-1]) for i in
                        range(0, max_Ngrams_n)]

    fig, axes = plt.subplots(ncols=2)
    plt.tight_layout()
    N = 100
    sns.barplot(y=df_Isis_ngrams[0][0].values[:N], x=df_Isis_ngrams[0][1].values[:N], ax=axes[0], color='blue')
    sns.barplot(y=df_random_ngrams[0][0].values[:N], x=df_random_ngrams[0][1].values[:N], ax=axes[1], color='orange')

    for i in range(2):
        axes[i].spines['right'].set_visible(False)
        axes[i].set_xlabel('')
        axes[i].set_ylabel('')
        axes[i].tick_params(axis='x', labelsize=10)
        axes[i].tick_params(axis='y', labelsize=10)

    axes[0].set_title('Top Isis Tweets unigrams', fontsize=12)
    axes[1].set_title('Top Random Tweets unigrams', fontsize=12)

    plt.show()
    # we can see that the text needs cleanup. Let's start


def plotFeatureDistribution(data):
    """
    examine distribution of diferent features as function of target
    :param data:
    """
    data.info()
    # attribute correlation (and separation)
    data_copy = data.copy()
    for col in ['description', 'name', 'username', 'tweets', 'Links', 'time',
                'followers', 'location', 'numberstatuses']:
        if col in data_copy.columns:
            data_copy.drop(columns=[col], inplace=True)
    data_debug = pd.concat([data_copy[-2000:-1], data_copy[2:2000]])
    sns.pairplot(data_debug, hue="label")
    plt.show()
    # the calculated features did not show significant performance and will not be used
