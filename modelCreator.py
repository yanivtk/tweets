import numpy as np
import os
import pickle
import keras
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import f1_score, classification_report
import tensorflow as tf
from plotter import inspectNgrams, plotFeatureDistribution  # data inspection helper functions
from dataPreparation import buildFeatures, load_data


class model_creator:
    def __init__(self, random_data_path='', isis_data_path='',
                 model_type='MLE', translate_text=False, manual_run=False,
                 save_data=True,
                 default_threshold=0.5,
                 label_0_subsample=0.1,
                 threshold_step=0.01,
                 embedding_dimension=20,  # 120,
                 max_n_chars=280,
                 lstm_units=64,
                 lstm_loss='binary_crossentropy',
                 lstm_optimizer='adam',
                 lstm_metrices='accuracy',
                 lstm_epochs=10,
                 lstm_validation_split=0.1,
                 hidden_layers_activation='relu',
                 output_layers_activation='sigmoid',
                 hidden_layers_units=24):
        """
        generate Isis/ random tweets classifier
        :param manual_run: bool enable manual data exploration
        :param translate_text: bool pass isis text through google translat
        :param random_data_path:
        :param isis_data_path:
        :param model_type: LSTM or naive Bayes
        :param default_threshold:
        :param label_0_subsample:
        :param threshold_step:
        :param embedding_dimension:
        :param max_n_chars:
        :param lstm_units:
        :param lstm_loss:
        :param lstm_optimizer:
        :param lstm_metrices:
        :param lstm_epochs:
        :param lstm_validation_split:
        :param hidden_layers_activation:
        :param output_layers_activation:
        :param hidden_layers_units:
        :param save_data:
        """
        self.save_data = save_data
        self.random_data_path = random_data_path
        self.isis_data_path = isis_data_path
        self.model_type = model_type
        self.translate_text = translate_text
        self.model = None
        self.manual_run = manual_run
        # params
        self.threshold_step = threshold_step
        self.label_0_subsample = label_0_subsample
        self.threshold = default_threshold
        # LSTM params
        self.embedding_dimension = embedding_dimension  # standard val
        self.max_n_chars = max_n_chars  # for padding
        self.lstm_units = lstm_units
        self.hidden_layers_activation = hidden_layers_activation
        self.output_layers_activation = output_layers_activation
        self.hidden_layers_units = hidden_layers_units
        self.lstm_loss = lstm_loss
        self.lstm_optimizer = lstm_optimizer
        self.lstm_metrices = lstm_metrices
        self.epochs = lstm_epochs
        self.validation_split = lstm_validation_split
        self.tokenizer = Tokenizer()
        self.train_set = None
        self.threshold_set = None
        self.test_set = None
        self.y = None
        self.y_test = None
        self.y_threshold = None
        self.data = []

    def get_model(self):
        if os.path.exists(r'models/saved_model.pb') and os.path.exists(r'models/tokenizer.pickle'):
            with open('models/tokenizer.pickle', 'rb') as tokenizer_file:
                tokenizer = pickle.load(tokenizer_file)
            model = tf.keras.models.load_model('models')
        else:
            model, tokenizer = self.create(r'data/tweets_isis_translated.xlsx', r'data/tweets_random_all.xlsx')
        return model, tokenizer

    def explore_data(self):
        buildFeatures(self.data)
        # feature exploration - evaluate our features
        inspectNgrams(self.data)  # no stopwords or weird chars - we're probably good
        plotFeatureDistribution(self.data)
        # various standard features did not show enough value, so I went with tf-idf

    def create(self, random_data_path, isis_data_path):
        self.random_data_path = random_data_path
        self.isis_data_path = isis_data_path

        self.data = load_data(self.random_data_path, self.isis_data_path)
        if self.manual_run:
            self.explore_data()

        self.y = self.data.label.copy()
        del (self.data['label'])

        X_train, X_test, y_train, self.y_test = train_test_split(self.data, self.y,
                                                                 test_size=0.3, random_state=5183)
        X_threshold, X_test, self.y_threshold, self.y_test = train_test_split(X_test,
                                                                              self.y_test,
                                                                              test_size=0.5, random_state=7872)

        if self.model_type.lower() in ['naive_bayes', 'naive bayes']:
            # naive bayes and tf-idf
            count_vectorizer = CountVectorizer()
            X_train_counts = count_vectorizer.fit_transform(X_train.tweets)
            X_test_counts = count_vectorizer.transform(X_test.tweets)

            # use the train set to fit the vocabulary, transform test accordingly
            tfidf_transformer = TfidfTransformer()
            X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
            X_test_tfidf = tfidf_transformer.transform(X_test_counts)
            naiveBayesModel = MultinomialNB().fit(X_train_tfidf, y_train)

            # predicted_prob = naiveBayesModel.predict_proba(X_test_tfidf)
            prediction = naiveBayesModel.predict(X_test_tfidf)
            print(classification_report(self.y_test, prediction, labels=[0, 1]), sep='\n')
            if self.save_data:
                pickle.dump(naiveBayesModel, open(r'models/MultinomialNB.dill', 'wb'))
            self.model = naiveBayesModel
        ######### currently lstm is the default
        else:
            # if self.model_type.lower() == 'lstm':
            self.tokenizer.fit_on_texts(self.data["tweets"])

            vocabulary_size = len(self.tokenizer.word_index) + 1

            self.train_set = pad_sequences(self.tokenizer.texts_to_sequences(X_train.tweets), padding='post',
                                           maxlen=self.max_n_chars)
            self.threshold_set = pad_sequences(self.tokenizer.texts_to_sequences(X_threshold.tweets), padding='post',
                                               maxlen=self.max_n_chars)
            self.test_set = pad_sequences(self.tokenizer.texts_to_sequences(X_test.tweets), padding='post',
                                          maxlen=self.max_n_chars)
            if self.save_data:
                with open('models/tokenizer.pickle', 'wb') as handle:
                    pickle.dump(self.tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)

            LSTMModel = keras.Sequential([
                keras.layers.Embedding(vocabulary_size, self.embedding_dimension, input_length=self.max_n_chars),
                keras.layers.Bidirectional(keras.layers.LSTM(self.lstm_units)),
                keras.layers.Dense(self.hidden_layers_units, activation=self.hidden_layers_activation),
                keras.layers.Dense(1, activation=self.output_layers_activation)
            ])

            LSTMModel.compile(loss=self.lstm_loss, optimizer=self.lstm_optimizer, metrics=[self.lstm_metrices])
            LSTMModel.fit(self.train_set, y_train, epochs=self.epochs, verbose=1,
                          validation_split=self.validation_split)

            # optimize threshold
            prediction = LSTMModel.predict(self.threshold_set)

            f1_scores = []
            thresholds = []

            for i in range(0, int(np.ceil(1 / self.threshold_step))):
                threshold = self.threshold_step * i
                # binarize label for accuracy calculation
                binary_prediction = [0 if x[0] < threshold else 1 for x in prediction]
                score = f1_score(self.y_threshold, binary_prediction, labels=[0, 1])
                f1_scores.append(score)
                thresholds.append(threshold)  # this part can be replaced by multiplying indices with threshold_step,
                # but this method is safer

            self.threshold = thresholds[np.where(f1_scores == max(f1_scores))[0][0]]
            self.test_model(LSTMModel, self.test_set, self.y_test, self.threshold)
            if self.save_data:
                tf.keras.models.save_model(LSTMModel, 'models')

            return LSTMModel, self.tokenizer

    def test_model(self, model, test_set, y_test, threshold=0.5):
        """
        Print report for a ml model
        :param model:
        :param test_set:
        :param y_test:
        :param threshold:
        """
        prediction = model.predict(test_set)
        binary_prediction = [0 if x[0] < threshold else 1 for x in prediction]
        print(classification_report(y_test, binary_prediction, labels=[0, 1]), sep='\n')
