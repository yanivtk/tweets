import requests
import json
from requests_oauthlib import OAuth1

def tweetParse(url, creds_filename):
    with open(creds_filename, 'r', encoding='utf-8', errors='ignore') as file:
        creds = json.load(file)

    auth = OAuth1(creds['YOUR_APP_KEY'],
                  creds['YOUR_APP_SECRET'],
                  creds['USER_OAUTH_TOKEN'],
                  creds['USER_OAUTH_TOKEN_SECRET'])
    tweet_data = requests.get(url, auth=auth)
    return tweet_data.text
