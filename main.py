from modelCreator import model_creator
from dataPreparation import predict
from TweeterParser import tweetparse
from flask import Flask, redirect, url_for, request, render_template

model, tokenizer = model_creator().get_model()

app = Flask(__name__)


@app.route("/")
def index():
    return render_template("Home.html")


@app.route('/probability/<message>')
def probability(message):
    return str(message)


@app.route('/success/<text>')
def success(text):
    return 'Result' + str(text)


@app.route('/enter', methods=['POST', 'GET'])
def enter_url():
    if request.method == 'POST':
        input_url = "https://twitter.com/i/web/status/" + request.form['input_id']
    else:
        input_url = "https://twitter.com/i/web/status/" + request.args.get('input_id')
    tw_text = tweetparse.tweetParse(input_url, 'creds.json')
    probability = predict(model, tokenizer, tw_text, max_n_chars=280)[0][0]

    text = '"' + tw_text + '"' + ". prob relation to Isis " + str(probability)
    return redirect(url_for('probability', message=text, url = input_url))


if __name__ == '__main__':
    # app.run(debug=True)
    app.run(host='0.0.0.0')
