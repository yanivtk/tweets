# Isis Vs random Tweet classifier #

This project runs a local http server which recives a tweter ID as an input, and returns the probability for it to be twitted by an Isis supporter.

### How to run ###

Download project and run main.py using python 3.8 (I used 3.8.6). The terminal will present the server's address, as a link. Open the link by pressing it.
This will open a browser page with a text box. Enter the ID and press submit. The screen will display the probability for it to be an Isis Tweet.

The server only runs locally as it redirects to local network addresses (This can be fixed, but requires more time). 

***Important note
Due to access limitions by twitter, standard web crawlers (i.e beuatifulsoap) are blocked and cannot be used for tweets collection. The two options I came up with were either using selenium
(which requires installation of a local driver per browser) or using the twitter API. I implemented both, but this package includes the latter (twitter API).
Running requires twitter devloper user_oauth_token and user_oauth_token_secret, as well as an app key and app_secret. These tokens are personal and cannot be shared.
==> The user is therefore required to open a developer account in twitter and insert the relevant credentials into creds.json in the code.

### Description ###
The code uses an LSTM classifier with tokenized text. I undersampled the random tweets, as there were O(10) more of them, compared to Isis.
The original data was paritally in arabic, so I used google translate (function translateTweetToEnglish() in the code)
I also implemnted a naive bayes classifier with tf-idf, but it's results weren't as good.
I made other attempts, but got lesser precision and recall (i.e. SVM, XGBoost with ngrams)

Many words in the data are extremly biased towards Isis (Syria, Israel, Allah.....). As a result, the classifier suffers from similar bias.

### Code structure ###
main
------
Includes the server app and main code. 
Loads the saved lstm model and tokenizer (It can also use the saved mutinomialNB (naive bayes) classifier).
It then runs the model on the tweet, retrived by the ID.

plotter.py 
----------
includes the functions I used for data exploration. Calculates n grams and draws histograms. 

modelCreator.py
-----------
creates and saves the model (LSTM or NB. If needed, I also have XGBoost and SVM examples, and half baked NN with Bert layer)

dataPreparation.py
----------------
loads the data, cleans and preprocess it. It also extracts standard features, but I ended up not using them as they were not good separators.

tweetparse.py
--------------
the tweet retriever (uses the tweeter API)

### The errors (LSTM, best one) ###

Data and validation sets are 0.2 of data

* label 0
precision 0.86 
recall 0.84
F1 0.85

* label 1
precision 0.93 
recall 0.94
F1 0.93

Accuracy			0.91


### Steps I would take given more time ###
1. Improve data cleaning - I did a lot of cleaning, but some typos and bogus chars are still there. 
2. Improve the bias towards certain words (I tried using ngrams, but it did not solve the problem). 
3. (Optional) use a more sophisticated model (I tried implementing a BERT layer, but it took too long so I gave up)
4. Implement a better tweet scrapper, which does not rely on tweetr API
5. Scrap much more data to improve the dataset
6. Maybe use emojies in the classification
7. Modify the server's rerouting addresses, to allow remote access